'use strict'

/**
 * Role
 * @description :: Model for storing Role records
 */

module.exports = {
  schema: true,

  description: 'Confers "Permission" to "User"',

  attributes: {
    name: {
      type: 'string',
      notNull: true,
      unique: true,
      enum: [
        'super',
        'administrator',
        'concierge',
        'owner',
        'tenant'
      ]
    },

    active: {
      type: 'boolean',
      defaultsTo: true
    },

    users: {
      collection: 'user',
      via: 'roles',
      through: 'userroles'
    },

    permissions: {
      collection: 'permission',
      via: 'roles',
      through: 'rolepermissions'
    },

    toJSON() {
      return this.toObject()
    }
  },

  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next(),

  seedData: [
    {'name': 'super','active': true},
    {'name': 'administrator','active': true},
    {'name': 'concierge', 'active': true},
    {'name': 'owner', 'active': true},
    {'name': 'tenant', 'active': true},
  ]
}
