'use strict';

/**
 * Person
 * @description :: Model for storing Person records
 */

module.exports = {
  schema: true,

  attributes: {
    name: {
      type: 'string',
      required: true,
      defaultsTo: ''
    },

    user:{
      model: 'user'
    },

    // Attribute methods
    getFullName(){
      return this.name;
    },

    toJSON() {
      let person = this.toObject();
      person.fullName = this.getFullName();
      return person
    }
  },

  beforeUpdate: (values, next) => next(),
  beforeCreate: (values, next) => next()
};
