'use strict';

/**
 * UserController
 * @description :: Server-side logic for manage users
 */
const _ = require('lodash');

module.exports = {
  full(req, res) {
    let id = req.param('id');
    if (_.isUndefined(id)) {
      User
        .find()
        .populate('person')
        .populate('roles')
        .then(res.ok)
        .catch(res.negotiate);
    } else {
      User
        .findOne({
          id: id
        })
        .populate('person')
        .populate('roles')
        .then(res.ok)
        .catch(res.negotiate);
    }
  },

  me(req, res) {
    User
      .findOne({
        id: req.user.id
      })
      .populate('person')
      .populate('roles')
      .then(res.ok)
      .catch(res.negotiate);
  }
};
