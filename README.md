# Comvive

## Description

API REST para la plataforma Comvive (para la gestión de comunidades)

## Installation

Clone the repository and run the following commands under your project root:

```shell
npm install
npm start
```
