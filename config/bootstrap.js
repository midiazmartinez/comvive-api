'use strict';

/**
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 * @param {Function} cb This function should always be called, so DON'T REMOVE IT
 */

const async = require('async');

module.exports = {

  bootstrap: cb => {

    async.series([
      Permission.seed,
      Role.seed,
      RolePermissions.seed
    ],cb);

    // It's very important to trigger this callback method when you are finished
    // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  }
};
